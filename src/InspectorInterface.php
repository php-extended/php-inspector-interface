<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-inspector-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Inspector;

use Stringable;

/**
 * InspectorInterface interface file.
 * 
 * This interface represents an engine capable of decomposing an arbitrarily 
 * complex variable into humanly intelligible parts.
 * 
 * @author Anastaszor
 */
interface InspectorInterface extends Stringable
{
	
	/**
	 * Gets whether this inspector equals another object.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean
	 */
	public function equals($object) : bool;
	
	/**
	 * Gets an inspected type from the given variable.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $variable
	 * @return InspectedTypeInterface
	 */
	public function inspect($variable) : InspectedTypeInterface;
	
}
