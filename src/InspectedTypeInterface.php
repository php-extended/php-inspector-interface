<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-inspector-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Inspector;

use Stringable;

/**
 * InspectedTypeInterface interface file.
 * 
 * This class represents an abstract synthesis of an arbitrary variable.
 * 
 * @author Anastaszor
 */
interface InspectedTypeInterface extends Stringable
{
	
	/**
	 * Gets whether this inspected type equals another object.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean
	 */
	public function equals($object) : bool;
	
	/**
	 * Gets a sample about the value that was captured. The length of the
	 * sample is left to the implementation.
	 * 
	 * @return string
	 */
	public function getSample() : string;
	
}
